package com.example.tempreture;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "YCOW", value = "/YangChangOwnWebsite")
public class YCOW extends HttpServlet {

    public void init() {}

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        try {
            if (!request.getParameter("c").isEmpty()) {
                Double.parseDouble(request.getParameter("c"));
                response.getWriter().println("tempreture from celsius to Fahrenheit is " +
                        (Double.parseDouble(request.getParameter("c")) * 1.8) + 32);
            } else {
                response.getWriter().println("Incorrect input");
            }
        } catch (NumberFormatException a){
            response.getWriter().println("Incorrect input");
        }

    }

    public void destroy() {
    }
}